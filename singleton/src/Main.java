public class Main {
    public static void main(String[] args) {
        Thread Hilo1 =new Thread(new Runnable() {
            @Override
            public void run() {
                Controlador controlador = Controlador.getControlador();
                System.out.println(controlador.hashCode());
            }
        });

        Thread Hilo2 =new Thread(new Runnable() {
            @Override
            public void run() {
                Controlador controlador2 = Controlador.getControlador();
                System.out.println(controlador2.hashCode());
            }
        });

        Hilo1.start();
        Hilo2.start();

    }
}