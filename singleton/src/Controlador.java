public class Controlador {

    private static Controlador controlador;
    private Controlador() {}


    //eL sync lo que hace es es que los Thread esperan a que este metodo sea liberado y asi solo limitar la creacion del objeto
    public synchronized static Controlador getControlador() {
        if(controlador == null){
            controlador=new Controlador();
        }
        return controlador;
    }
}
